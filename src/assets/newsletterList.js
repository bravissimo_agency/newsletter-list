(function () {
    document.querySelectorAll('.newsletterList__deleteButton')
        .forEach(button => button.addEventListener('click', removeEmail));

    async function removeEmail() {
        const button = this;
        const element = button.closest('.newsletterList__card');
        const id = button.dataset.id;

        if (element.classList.contains('isLoading')) {
            return;
        }

        element.classList.add('isLoading');

        try {
            const res = await fetch(ajaxurl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                },
                body: `action=newletter_list_remove_email&_wpnonce=${window.custom_nonce}&id=${id}`,
                credentials: 'same-origin'
            });

            const data = await res.json();

            if (data) {
                element.remove();
                alert('Email deleted successfully');
            } else {
                showErrorMessage();
            }
        } catch (error) {
            showErrorMessage();
        }

        element.classList.remove('isLoading');
    }

    function showErrorMessage() {
        alert('Something went wrong, please try again.');
    }
})(); 