<?php

namespace NewsletterList;

class NewsletterList {
    protected $settings = [
        'page_name' => 'Newsletter list',
        'permission' => 'edit_pages',
        'page_slug' => 'newsletter-list',
        'success_message' => 'success'
    ];

    public function init($settings = []) {
        $this->settings = array_merge($this->settings, $settings);

        add_action('init', function () {
            register_post_type('newsletterSubmit', [
                'labels' => [
                    'name' => __('Newsletter submits'),
                    'singular_name' => __('Newsletter submit')
                ],
                'public' => false,
                'has_archive' => false,
                'show_ui' => false,
                'show_in_menu' => false,
            ]);
        });

        add_action('admin_menu', [$this, 'addMenuPage']);
        add_action('rest_api_init', [$this, 'initEndPoint']);
        add_action('wp_ajax_newletter_list_remove_email', [$this, 'removeEmail']);
    }

    public function addMenuPage() {
        add_menu_page(
            $this->settings['page_name'],
            $this->settings['page_name'],
            $this->settings['permission'],
            $this->settings['page_slug'],
            [$this, 'showNewsletterList'],
            '',
            100
        );   
    }

    public function removeEmail() {
        $submitId = (int) $_POST["id"];

        if (get_post_type($submitId) !== 'newsletterSubmit') {
            wp_delete_post($submitId);
            wp_send_json(1);
            wp_die();
        }
        
        wp_send_json(0);
        wp_die();
    }

    public function initEndPoint() {
        register_rest_route('/api', '/newsletter-list', [
            'methods' => 'POST',
            'callback' => function($request) {
                $request = $request->get_params();

                wp_insert_post([
                    'post_title' => $request['email'],
                    'post_type' => 'newsletterSubmit',
                    'post_status' => 'private'
                ]);

                return $this->settings['success_message'];
            },
        ]);
    }

    public function showNewsletterList() {
        $submits = get_posts([
            'post_type' => 'newsletterSubmit',
            'post_status' => 'private',
            'posts_per_page' => -1,
        ]);
        $style = file_get_contents(__DIR__ . '/assets/newsletterList.css');
        $js = file_get_contents(__DIR__ . '/assets/newsletterList.js');
        ?>

        <style>
            <?= $style ?>
        </style>
    
        <div id="newsletterList">
            <?php foreach ($submits as $submit) : ?>
                <div class="newsletterList__card card">
                    <?= $submit->post_title ?>

                    <div>
                        <span class="spinner"></span>
                        <button 
                            class="newsletterList__deleteButton button button-primary" 
                            data-id="<?= $submit->ID ?>"
                        >
                            Delete
                        </button>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <script>
            <?= $js ?>
        </script>
        <?php
    }
}